Write('<h1>Contenido</h1>\r\n')
Write('<h2>%d</h2>\r\n' % {GetDate()})

dir = '/zip/www'

Write('<ul>\r\n')
for name, kind, ino, off in assert(unix.opendir(dir)) do
    if name ~= '.' and name ~= '..' then
        Write('<li><a href=www/%s>%s</a></li>\r\n' % {EscapeHtml(name), EscapeHtml(name)})
    end
end
Write('</ul>\r\n')
