#!/bin/bash

set -e

REDBEAN_PATH="$1"
URL=$2

TMP_PATH=$(mktemp -d -t immweb_XXXXXXXXXX)
mkdir "$TMP_PATH/www"

wget_rc=0

wget                       \
    --mirror               \
    --convert-links        \
    --adjust-extension     \
    --page-requisites      \
    --no-parent            \
    --no-if-modified-since \
    --quiet                \
    --show-progress        \
    --directory-prefix="$TMP_PATH/www" \
    $URL || wget_rc=$?

# return code 8 is not our problem
[[ $wget_rc == 8 ]] && { echo "WARNING: algunas cosas no se bajaron bien"; }
! [[ "0 8" =~ $wget_rc ]] && { echo "Falló wget ($wget_rc)"; exit $wget_rc; }

echo "Metiendo $TMP_PATH en $REDBEAN_PATH"
AUX=$(pwd)
cd "$TMP_PATH"
zip -r $AUX/$REDBEAN_PATH *
