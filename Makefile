redbean-2.2.com: checksums
	@curl -o $@ https://redbean.dev/$@
	@sha256sum --check checksums || rm -f $@
	@chmod +x $@

# To create a new checksum:
# 	$ sha256sum redbean-2.2.com >> checksums

# TODO: lograr que se actualice "en caliente"
portal: redbean-2.2.com content.lua
	@zip redbean-2.2.com content.lua

clean:
	@rm -f redbean-2.2.com

.PHONY: clean portal
